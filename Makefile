EXEC   = lycrt

SRCS   = main.c emission.c init.c mcrt.c output.c source.c spectrum.c transport.c tree.c

OBJS   = $(SRCS:.c=.o)
INCL   = allvar.h proto.h

GSL_INC = /usr/local/gsl/include
GSL_LIB = /usr/local/gsl/lib

HDF5_INC = /usr/local/hdf5/include
HDF5_LIB = /usr/local/hdf5/lib

CFLAGS =  -fPIC -O3 -g -ffast-math #  -Wall
CFLAGS += -Qunused-arguments

INCOPT =  -I$(GSL_INC) -I$(HDF5_INC)
LIBOPT =  -L$(GSL_LIB) -lgsl -lgslcblas -L$(HDF5_LIB) -lhdf5 -lhdf5_hl

LIBS   =  #-lm

CC     =  mpicc

$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $(INCOPT) $(LIBOPT) $(OBJS) -o $(EXEC)

.c.o:
	$(CC) $(CFLAGS) $(INCOPT) $(LIBOPT) -c -o $@ $<

$(OBJS): $(INCL)


clean:
	rm -rf $(OBJS) $(EXEC)
