#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"
#include "proto.h"


void read_octree_from_file(char* fname)
{
    if (ThisTask==0)
    {
        printf("------------------------------------\n\n");
        printf("# Reading octree from file %s\n\n", fname);
    }
    
    long i,j,k;
    int *buf_int;
    long *buf_long;
    double *buf_double;
    
    hid_t file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t status;
    
    status = H5LTget_attribute_long(file_id,"/","Ncell",&TOTAL_NUMBER_OF_CELLS);
    status = H5LTget_attribute_long(file_id,"/","Nparent",&TOTAL_NUMBER_OF_PARENTS);
    status = H5LTget_attribute_long(file_id,"/","Nleaf",&TOTAL_NUMBER_OF_LEAVES);
    status = H5LTget_attribute_double(file_id,"/","Boxsize",&MAX_DISTANCE_FROM0);
    MAX_DISTANCE_FROM0 *= All.UnitLength_in_cm; // convert units
    
    // allocate cell memory
    if (!(CELL=malloc(TOTAL_NUMBER_OF_CELLS*sizeof(struct CELL_STRUCT))))
    {
        if (ThisTask==0)
            printf("Failed to allocate memory (CELL).\n");
        endrun(4);
    }
    
    LEAF_IDS = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(long));
    PARENT_IDS = malloc(TOTAL_NUMBER_OF_PARENTS*sizeof(long));
    
    buf_int = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(int));
    status = H5LTread_dataset_int(file_id,"/sub_cell_check",&buf_int[0]);
    
    for (i=j=k=0; i<TOTAL_NUMBER_OF_CELLS; i++)
    {
        CELL[i].sub_cell_check = buf_int[i];
        if (CELL[i].sub_cell_check==1) {
            CELL[i].sub_cell_IDs = malloc(8*sizeof(long));
            PARENT_IDS[j] = i; j++; }
        else {
            CELL[i].U = malloc(sizeof(LOCALVAL));
            LEAF_IDS[k] = i; k++; }
    }
    
    free(buf_int); // sub_cell_check
    
    buf_double = malloc(3*TOTAL_NUMBER_OF_CELLS*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/min_x",buf_double);
    
    for (i=0; i<TOTAL_NUMBER_OF_CELLS; i++)
        for (j=0; j<3; j++)
            CELL[i].min_x[j] = buf_double[3*i+j]*All.UnitLength_in_cm;
    
    free(buf_double); // x_min
    
    buf_double = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/width",buf_double);
    
    for (i=0; i<TOTAL_NUMBER_OF_CELLS; i++)
        CELL[i].width = buf_double[i]*All.UnitLength_in_cm;
    
    free(buf_double); // width
    
    buf_long = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(long));
    status = H5LTread_dataset_long(file_id,"/parent_ID",buf_long);
    
    for (i=0; i<TOTAL_NUMBER_OF_CELLS; i++)
        CELL[i].parent_ID = buf_long[i];
    
    free(buf_long); // parent_ID
    
    buf_long = malloc(8*TOTAL_NUMBER_OF_PARENTS*sizeof(long));
    status = H5LTread_dataset_long(file_id,"/sub_cell_IDs",buf_long);
    
    for (i=0; i<TOTAL_NUMBER_OF_PARENTS; i++)
        for (j=0; j<8; j++)
            CELL[PARENT_IDS[i]].sub_cell_IDs[j] = buf_long[8*i+j];
    
    free(buf_long); // sub_cell_IDs
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/rho",buf_double);
    
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
        CELL[LEAF_IDS[i]].U->rho = buf_double[i]*All.UnitMass_in_g/pow(All.UnitLength_in_cm,3);
    
    free(buf_double); // rho
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/z",buf_double);
    
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
        CELL[LEAF_IDS[i]].U->z = buf_double[i];
    
    free(buf_double); // z
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/T",buf_double);
    
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
        CELL[LEAF_IDS[i]].U->T = buf_double[i];
    
    free(buf_double); // T
    
    if (ThisTask==0)
    {
        printf("Octree domain size = %g\n", MAX_DISTANCE_FROM0);
        printf("Total %ld cells in tree, %ld leaves\n\n", TOTAL_NUMBER_OF_CELLS, TOTAL_NUMBER_OF_LEAVES);
    }
    
    H5Fclose(file_id);
    
    // initialization
    double rho_min = 1e-10*pc.mp/All.HydrogenFraction;
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
    {
        if (CELL[LEAF_IDS[i]].U->rho<rho_min)
            CELL[LEAF_IDS[i]].U->rho = rho_min; // no zero density
        if (CELL[LEAF_IDS[i]].U->T<10)
            CELL[LEAF_IDS[i]].U->T = 10; // no cooler than 10 K
        if (CELL[LEAF_IDS[i]].U->T>1e6)
            CELL[LEAF_IDS[i]].U->z = 0; // no dust above 10^6 K
        
        CELL[LEAF_IDS[i]].U->nh = 1e-10; // start with fully ionized
    }
}


long find_cell_from_cell(double pos[3], long cell_ID_0)
{
    // check if outside domain
    int j;
    for (j=0; j<3; j++)
        if (pos[j]<-MAX_DISTANCE_FROM0 || pos[j]>=MAX_DISTANCE_FROM0)
            return -1;
    
    // invalid cell index
    if ((cell_ID_0<0) || (cell_ID_0>=TOTAL_NUMBER_OF_CELLS))
        cell_ID_0 = 0;
    
    // check if already in cell
    int in_cell_check=1;
    for (j=0; j<3; j++)
        if (pos[j] >= CELL[cell_ID_0].min_x[j] + CELL[cell_ID_0].width ||
            pos[j] < CELL[cell_ID_0].min_x[j]) in_cell_check = 0;
    
    // find parent cell it belongs
    long cell_ID = cell_ID_0;
    while (in_cell_check==0)
    {
        cell_ID = CELL[cell_ID].parent_ID;
        
        in_cell_check = 1;
        for (j=0; j<3; j++)
            if (pos[j] >= CELL[cell_ID].min_x[j] + CELL[cell_ID].width ||
                pos[j] < CELL[cell_ID].min_x[j]) in_cell_check = 0;
    }
    
    // find the finest-level cell
    int n_x[3], n_sub_ID;
    while (CELL[cell_ID].sub_cell_check==1)
    {
        for (j=0; j<3; j++)
            n_x[j] = (int)((pos[j]-CELL[cell_ID].min_x[j])/(CELL[cell_ID].width/N_SUBCELL));
        
        n_sub_ID = n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
        cell_ID = CELL[cell_ID].sub_cell_IDs[n_sub_ID];
    }
    return cell_ID;
}


double maxstep_escape_cell(double* pos, double* n_hat, long cell_id)
{
    int j;
    double dr_i;
    double dr = 0.5*CELL[cell_id].width;
    double eps = 1e-10*MAX_DISTANCE_FROM0;
    
    for (j=0; j<3; j++)
        if (n_hat[j]!=0)
        {
            dr_i = (CELL[cell_id].min_x[j]-pos[j])/n_hat[j];
            if ((dr_i>=0) && (dr_i<dr)) dr = dr_i;
            dr_i += (CELL[cell_id].width)/n_hat[j];
            if ((dr_i>=0) && (dr_i<dr)) dr = dr_i;
        }
    
    return dr+eps;
}


void wipe_J_UV(void)
{
    long i;
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
        CELL[LEAF_IDS[i]].U->J_UV = 0;
}


void reduce_J_UV(void)
{
    long i; double buf;
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
    {
        MPI_Allreduce(&CELL[LEAF_IDS[i]].U->J_UV, &buf, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        CELL[LEAF_IDS[i]].U->J_UV = buf/pow(CELL[LEAF_IDS[i]].width,3); // in # s^-1
    }
}


void compute_ionization_state(void)
{
    long i;
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
    {
        // photoionization rate
        double JN = CELL[LEAF_IDS[i]].U->J_UV/(All.HydrogenFraction*CELL[LEAF_IDS[i]].U->rho/pc.mp);
        
        // collisional ionization coefficient from Dan's code
        //double t    = CELL[n_cell].U->T;
        //double zeta = Rydberg_in_eV/Boltzman_k/t;
        //double Th   = pow(t,-1.5);
        //double f    = 0.6*2.7/zeta/zeta*Th*exp(-zeta);
        
        // collisional ionization coefficient from KWH96 (in GIZMO)
        double t = CELL[LEAF_IDS[i]].U->T;
        double f = 5.85e-11*sqrt(t)*exp(-157809.1/t)/(1.0+sqrt(t/1.0e5));
        
        // recombination coefficient from Verner (alpha A)
        //double av    = 7.982e-11;
        //double bv    = 0.7480;
        //double T0_v  = 3.148;
        //double T1_v  = 7.036e5l;
        //double t0v   = sqrt(t/T0_v);
        //double t1v   = sqrt(t/T1_v);
        //double alpha = av/(t0v*pow(1+t0v,1-bv)*pow(1+t1v,1+bv));
        
        // recombination coefficient from Hui & Gnedin (1997)
        double lam_H = 2*157807./t;
        double fact  = pow(1+pow(lam_H/2.740,0.407),2.242);
        double alpha = 2.753e-14*pow(lam_H,1.5)/fact; // case B
        
        // solve the equation: alpha*[(1-x)*n]^2 = f*(x*n)*[(1-x)*n] + J_UV*x*n
        double b = (2*alpha+f+JN)/(alpha+f);
        double c = 4*alpha/(alpha+f);
        double det = b*b - c;
        if (det<0) det = 0;
        CELL[LEAF_IDS[i]].U->nh = (b-sqrt(det))/2; // check back later
        if (CELL[LEAF_IDS[i]].U->nh<1e-10) CELL[LEAF_IDS[i]].U->nh = 1e-10; // protect
    }
}


void free_octree()
{
    long i;
    for (i=0; i<TOTAL_NUMBER_OF_CELLS; i++)
    {
        if (CELL[i].sub_cell_check==1)
            free(CELL[i].sub_cell_IDs);
        else
            free(CELL[i].U);
    }
    free(CELL);
    free(LEAF_IDS);
    free(PARENT_IDS);
}
