#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "proto.h"

double E_emit_start = 13.605693009;
double E_emit_stop = 24.4;

void emit_from_source(Ray_struct *ray)
{
    long source_id = sample_cdf(CDF_source);
    ray->source_id = source_id;
    ray->pos[0] = SOURCE[source_id].pos[0];
    ray->pos[1] = SOURCE[source_id].pos[1];
    ray->pos[2] = SOURCE[source_id].pos[2];
    ray->cell_id = SOURCE[source_id].cell_id;
    SOURCE[source_id].Nemit++;
    
    ray->Q = SOURCE[source_id].Q/CDF_source.pdf[source_id]/All.n_photons_star;
    //ray->E = E_emit_start*pow(10,log10(E_emit_stop/E_emit_start)*gsl_rng_uniform(random_generator));
    ray->E = pc.chi;
    
    // isotropic emission
    ray->mu = -1+2.0*gsl_rng_uniform(random_generator);
    ray->phi = 2.0*pc.pi*gsl_rng_uniform(random_generator);
    double sin_theta = sqrt(1-ray->mu*ray->mu);
    ray->n_hat[0] = sin_theta*cos(ray->phi);
    ray->n_hat[1] = sin_theta*sin(ray->phi);
    ray->n_hat[2] = ray->mu;
    
    ray->escape = 0;
    ray->killed = 0;
    ray->source = 1;
}


void emit_from_uvb(Ray_struct *ray)
{
    double mu, phi, sin_theta;
    
    // choose emission sphere just outside cubical domain
    double r_emit = pc.sqrt3*MAX_DISTANCE_FROM0;
    
    ray->killed = 1;
    while (ray->killed==1)
    {
        // pick initial position on photosphere
        double phi_core = 2*pc.pi*gsl_rng_uniform(random_generator);
        double cosp_core = cos(phi_core);
        double sinp_core = sin(phi_core);
        double cost_core = 1-2.0*gsl_rng_uniform(random_generator);
        double sint_core = sqrt(1-cost_core*cost_core);
        // pick photon propogation direction wtr to local normal
        double phi_loc = 2*pc.pi*gsl_rng_uniform(random_generator);
        // choose sqrt(R) to get outward, cos(theta) emission
        double cost_loc  = sqrt(gsl_rng_uniform(random_generator));
        double sint_loc  = sqrt(1-cost_loc*cost_loc);
        // local direction vector
        double D_xl = sint_loc*cos(phi_loc);
        double D_yl = sint_loc*sin(phi_loc);
        double D_zl = cost_loc;
        // real coordinates
        ray->pos[0] = r_emit*sint_core*cosp_core;
        ray->pos[1] = r_emit*sint_core*sinp_core;
        ray->pos[2] = r_emit*cost_core;
        // apply rotation matrix to convert vector into overall frame
        ray->n_hat[0] = cost_core*cosp_core*D_xl - sinp_core*D_yl + sint_core*cosp_core*D_zl;
        ray->n_hat[1] = cost_core*sinp_core*D_xl + cosp_core*D_yl + sint_core*sinp_core*D_zl;
        ray->n_hat[2] = -sint_core*D_xl +  cost_core*D_zl;
        // flip it inward
        ray->n_hat[0] = -1*ray->n_hat[0];
        ray->n_hat[1] = -1*ray->n_hat[1];
        ray->n_hat[2] = -1*ray->n_hat[2];
        
        // check if it can enter the domain
        int j; double xyz[3]; double nx[3];
        for (j=0;j<3;j++) {xyz[j]=ray->pos[j]; nx[j]=ray->n_hat[j];}
        double xmin, xmax, lmin=0;
        double lmax = 10*MAX_DISTANCE_FROM0;
        double L = MAX_DISTANCE_FROM0;
        double eps = 1e-10*MAX_DISTANCE_FROM0;
        
        for (j=0;j<3;j++)
        {
            if (nx[j]==0) {xmin=1e10; xmax=-1e10;}
            if (nx[j]>0) {xmin=(-L-xyz[j])/nx[j]; xmax=(L-xyz[j])/nx[j];}
            if (nx[j]<0) {xmin=(L-xyz[j])/nx[j]; xmax=(-L-xyz[j])/nx[j];}
            if (lmin<xmin) lmin=xmin;
            if (lmax>xmax) lmax=xmax;
        }
        
        if (lmax>lmin) // yes, it does
        {
            for (j=0;j<3;j++) ray->pos[j] += (lmin+eps)*ray->n_hat[j];
            ray->cell_id = find_cell_from_cell(ray->pos,0);
            if (ray->cell_id!=-1) // protect for double precision
            {
                ray->Q = Q_uvb/All.n_photons_uvb;
                //ray->E = E_emit_start+(E_emit_stop-E_emit_start)*gsl_rng_uniform(random_generator);
                ray->E = pc.chi;
                ray->escape=0;
                ray->killed=0;
                ray->source=0;
            }
        }
    }
}
