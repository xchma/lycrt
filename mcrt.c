#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "proto.h"


void run_mcrt(void)
{
    time_t start_tp, end_tp; float time_wasted;
    int i_iter; long ray_num, n_rays;

    for (i_iter=0; i_iter<All.n_iters; i_iter++)
    {
        // initialize
        if (ThisTask==0)
        {
            printf("------------------------------------\n\n");
            printf("# Doing %d interation\n\n", 1+i_iter);
        }
        wipe_J_UV();
        wipe_source();
        wipe_spectrum();
        
        // doing sources
        if (ThisTask==0)
        {
            printf("Total %ld photons emitted from sources.\n", All.n_photons_star);
            time(&start_tp);
        }

        n_rays = All.n_photons_star/NTask;
        for (ray_num=0; ray_num<n_rays; ray_num++)
        {
            Ray_struct ray;
            emit_from_source(&ray);
            integrate_ray_to_escape(&ray);
        }
        
        if (ThisTask==0)
        {
            time(&end_tp);
            time_wasted=difftime(end_tp,start_tp)/60;
            printf("Time costed: %.2f minutes.\n\n", time_wasted);
        }
        
        // doing UV background
        if (ThisTask==0)
        {
            printf("Total %ld photons emitted from UV background\n", All.n_photons_uvb);
            time(&start_tp);
        }
        
        n_rays = All.n_photons_uvb/NTask;
        for (ray_num=0; ray_num<n_rays; ray_num++)
        {
            Ray_struct ray;
            emit_from_uvb(&ray);
            integrate_ray_to_escape(&ray);
        }
        
        if (ThisTask==0)
        {
            time(&end_tp);
            time_wasted=difftime(end_tp,start_tp)/60;
            printf("Time costed: %.2f minutes.\n\n", time_wasted);
        }
        
        // reduce and update ionization states
        if (ThisTask==0)
        {
            printf("Reduce intensity and update ionization state\n");
            time(&start_tp);
        }
        
        reduce_J_UV();
        compute_ionization_state();
        reduce_source();
        reduce_spectrum();
        
        if (ThisTask==0)
        {
            time(&end_tp);
            time_wasted=difftime(end_tp,start_tp)/60;
            printf("Time costed: %.2f minutes.\n\n", time_wasted);
        }
        
        // write output
        if (ThisTask==0)
        {
            write_spectrum(i_iter);
            if (i_iter==All.n_iters-1) write_ionization_state();
            if (i_iter==All.n_iters-1) write_source();
        }
    }
    
    return;
}
