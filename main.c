#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/stat.h>
//#include <omp.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "proto.h"

int main(int argc, char** argv)
{
    // initialize MPI
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &ThisTask);
    MPI_Comm_size(MPI_COMM_WORLD, &NTask);
    
    if (argc<2)
    {
        if (ThisTask==0) printf("Usage: lycrt <ParamFile>\n");
        endrun(1);
    }
    
    if (ThisTask==0)
    {
        printf("------------------------------------\n\n");
        printf("# Running Lyman-continuum Radiative Transfer\n\n");
        printf("Using %d MPI Tasks\n\n", NTask);
    }
    
    // start timer
    double proc_time_start = MPI_Wtime();
    
    // random number generator
    random_generator = gsl_rng_alloc(gsl_rng_ranlxd1);
    unsigned long gsl_rng_default_seed=(unsigned int)time(NULL)+ThisTask;
    gsl_rng_set(random_generator, gsl_rng_default_seed);
    
    // read parameter file
    char ParamFile[200];
    strcpy(ParamFile, argv[1]);
    read_parameter_file(ParamFile);
    mkdir(All.OutputDir, 02755);
    
    // set up constants
    fill_in_physical_constant();
    
    // initialize tree, source, spectrum
    read_octree_from_file(All.OctreeFile);
    read_source_from_file(All.SourceFile);
    initialize_spectrum();
    
    
    // run mcrt
    run_mcrt();
    
    // clean up
    free_spectrum();
    free_source();
    free_octree();
    
    // calculate the elapsed time
    double proc_time_end = MPI_Wtime();
    double time_wasted = proc_time_end - proc_time_start;
    if (ThisTask==0)
        printf("\nRadiative transfer finished, %.2f hours used.\n\n", time_wasted/3600);
    
    // finalize
    MPI_Finalize();
}


void endrun(int ierr)
{
    if (ThisTask==0)
        printf("Endrun called with an error level of %d\n", ierr);
    MPI_Finalize();
    exit(0);
}
