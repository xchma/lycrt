// in emission.c
void emit_from_source(Ray_struct*);
void emit_from_uvb(Ray_struct*);

// in init.c
void read_parameter_file(char*);
void fill_in_physical_constant(void);

// in main.c
void endrun(int);

// in mcrt.c
void run_mcrt(void);

// in output.c
void write_ionization_state(void);
void write_source(void);
void write_spectrum(int);

// in source.c
void read_source_from_file(char*);
long sample_cdf(CDF);
void wipe_source(void);
void reduce_source(void);
void free_source(void);

// in spectrum.c
void initialize_spectrum(void);
void wipe_spectrum(void);
void reduce_spectrum(void);
void free_spectrum(void);

// in transport.c
void integrate_ray_to_escape(Ray_struct*);

// in tree.c
void read_octree_from_file(char*);
long find_cell_from_cell(double*, long);
double maxstep_escape_cell(double*, double*, long);
void wipe_J_UV(void);
void reduce_J_UV(void);
void compute_ionization_state(void);
void free_octree(void);
