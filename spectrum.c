#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"
#include "proto.h"


void initialize_spectrum(void)
{
    int N_los = All.n_mu*All.n_phi;
    spectrum = malloc(N_los*sizeof(double));
}


void wipe_spectrum(void)
{
    int i;
    for (i=0; i<All.n_mu*All.n_phi; i++)
        spectrum[i] = 0;
}


void reduce_spectrum(void)
{
    int i; double buffer;
    for (i=0;i<All.n_mu*All.n_phi;i++)
    {
        MPI_Allreduce(&spectrum[i], &buffer, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        spectrum[i] = buffer;
    }
}


void free_spectrum(void)
{
    free(spectrum);
}
