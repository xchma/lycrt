#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"
#include "proto.h"


void write_source(void)
{
    char fname[200];
    sprintf(fname, "%s/source.hdf5", All.OutputDir);
    
    // write source to HDF5 file
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    long j;
    long *buf_emit = malloc(CDF_source.n*sizeof(long));
    long *buf_escape = malloc(CDF_source.n*sizeof(long));
    for (j=0; j<CDF_source.n; j++)
    {
        buf_emit[j] = SOURCE[j].Nemit;
        buf_escape[j] = SOURCE[j].Nescape;
    }
    
    hsize_t dims1[1] = {CDF_source.n};
    status = H5LTmake_dataset_long(file_id, "Nemit", 1, dims1, buf_emit);
    status = H5LTmake_dataset_long(file_id, "Nescape", 1, dims1, buf_escape);
    
    free(buf_emit);
    free(buf_escape);
    H5Fclose(file_id);
}


void write_ionization_state(void)
{
    char fname[200];
    sprintf(fname, "%s/state.hdf5", All.OutputDir);
    
    // write ionization state to HDF5 file
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    long i;
    double *buf_nh = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    double *buf_JUV = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    
    for (i=0; i<TOTAL_NUMBER_OF_LEAVES; i++)
    {
        buf_nh[i] = CELL[LEAF_IDS[i]].U->nh;
        buf_JUV[i] = CELL[LEAF_IDS[i]].U->J_UV;
    }
    
    hsize_t dims1[1] = {TOTAL_NUMBER_OF_LEAVES};
    status = H5LTmake_dataset_double(file_id, "nh", 1, dims1, buf_nh);
    status = H5LTmake_dataset_double(file_id, "JUV", 1, dims1, buf_JUV);
    
    free(buf_nh);
    free(buf_JUV);
    H5Fclose(file_id);
}


void write_spectrum(int i_iter)
{
    char fname[200];
    sprintf(fname, "%s/spectrum_%02d.hdf5", All.OutputDir, i_iter);
    
    // write spectrum to HDF5 file
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    // write arributes
    hid_t hdf5_dataspace = H5Screate(H5S_SCALAR);
    hid_t hdf5_attribute = H5Acreate2(file_id, "n_mu", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &All.n_mu);
    
    hdf5_attribute = H5Acreate2(file_id, "n_phi", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &All.n_phi);
    
    hdf5_attribute = H5Acreate2(file_id, "Qion", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &CDF_source.Q);
    
    hsize_t dims1[1] = {All.n_mu*All.n_phi};
    status = H5LTmake_dataset_double(file_id, "spectrum", 1, dims1, spectrum);
    
    H5Fclose(file_id);
}

