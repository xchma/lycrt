#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <hdf5.h>
#include <hdf5_hl.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "proto.h"


void read_source_from_file(char* fname)
{
    if (ThisTask==0)
    {
        printf("------------------------------------\n\n");
        printf("# Reading sources from file %s\n\n", fname);
    }
    
    long i,j,N_source;
    double *buf_pos;
    double *buf_Qion;
    
    hid_t file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t status;
    
    // number of sources in file
    status = H5LTget_attribute_long(file_id,"/","Nsource",&N_source);
    if (ThisTask==0)
        printf("Total number of sources in file = %ld\n", N_source);
    
    buf_pos = malloc(3*N_source*sizeof(double));
    buf_Qion = malloc(N_source*sizeof(double));
    status = H5LTread_dataset_double(file_id, "/pos", buf_pos);
    status = H5LTread_dataset_double(file_id, "/Qion", buf_Qion);
    
    // allocate memory for sources
    SOURCE = malloc(N_source*sizeof(struct SOURCE_s));
    
    CDF_source.n = 0;
    for (i=0; i<N_source; i++)
    {
        double pos[3];
        for (j=0; j<3; j++) pos[j] = buf_pos[3*i+j]*All.UnitLength_in_cm;
        long cell_id = find_cell_from_cell(pos, 0);
        
        if (cell_id>-1)
        {
            SOURCE[CDF_source.n].pos[0] = pos[0];
            SOURCE[CDF_source.n].pos[1] = pos[1];
            SOURCE[CDF_source.n].pos[2] = pos[2];
            SOURCE[CDF_source.n].Q = buf_Qion[i];
            SOURCE[CDF_source.n].cell_id = cell_id;
            CDF_source.n++;
        }
    }
    
    free(buf_pos);
    free(buf_Qion);
    H5Fclose(file_id); // close file
    
    if (ThisTask==0)
    {
        printf("Total number of sources in domain = %ld\n", CDF_source.n);
        printf("\nSource file successfully read.\n\n");
    }
    
    // CDF for sources
    CDF_source.Q = 0; CDF_source.w = 0;
    CDF_source.pdf = malloc(CDF_source.n*sizeof(double));
    CDF_source.cdf = malloc(CDF_source.n*sizeof(double));
    for (j=0; j<CDF_source.n; j++)
    {
        // weight photon sampling by sqrt(Q)
        CDF_source.pdf[j] = sqrt(SOURCE[j].Q);
        CDF_source.cdf[j] = CDF_source.w + CDF_source.pdf[j];
        CDF_source.w += sqrt(SOURCE[j].Q);
        CDF_source.Q += SOURCE[j].Q;
    }
    for (j=0; j<CDF_source.n; j++)
    {
        CDF_source.pdf[j] /= CDF_source.w;
        CDF_source.cdf[j] /= CDF_source.w;
    }
    
    // total number of photons from the UVB
    Q_uvb = pc.pi*All.J_uvb*6*pow(2*MAX_DISTANCE_FROM0,2)/pc.h;
    
    if (ThisTask==0)
    {
        printf("Total ionizing photon emissivity %g per second from sources.\n", CDF_source.Q);
        printf("Total ionizing photon emissivity %g per second from UVB.\n\n", Q_uvb);
    }
}


long sample_cdf(CDF p)
{
    double z = gsl_rng_uniform(random_generator);
    
    // mid, lower, and upper points
    long bm, bl=0, bu=p.n-1;
    
    // check if off the top or bottom
    if (z>p.cdf[bu]) return bu;
    if (z<p.cdf[bl]) return bl;
    
    // search
    while (bu-bl>1)
    {
        bm = (bu+bl)/2;
        if (z>p.cdf[bm]) bl = bm;
        else bu = bm;
    }
    
    return bu;
}


void wipe_source(void)
{
    long j;
    for (j=0;j<CDF_source.n;j++)
    {
        SOURCE[j].Nemit=0;
        SOURCE[j].Nescape=0;
    }
}


void reduce_source(void)
{
    long j, buf_long;
    for (j=0; j<CDF_source.n; j++)
    {
        MPI_Allreduce(&SOURCE[j].Nemit, &buf_long, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
        SOURCE[j].Nemit = buf_long;
        MPI_Allreduce(&SOURCE[j].Nescape, &buf_long, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
        SOURCE[j].Nescape =buf_long;
    }
}


void free_source(void)
{
    free(SOURCE);
    free(CDF_source.cdf);
}
