#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_rng.h>

// MPI
int ThisTask, NTask;
// OpenMP
int maxThreads;

struct PhysicalConstant
{
    double pi;          // pi
    double sqrt3;       // sqrt(3)
    
    double c;           // speed of light
    double h;           // Planck constant
    double kB;          // Boltzmann constant
    
    double e;           // electron charge
    double mp;          // proton mass
    double me;          // electron mass
    double eV;          // electron Volt in erg
    
    double chi;         // HI ionization potential in eV
} pc;

// gsl random number
gsl_rng *random_generator;

// parameter type
#define MAXTAGS 50
#define INT     0
#define LONG    1
#define DOUBLE  2
#define STRING  3

// parameter file
struct Parameters
{
    char OctreeFile[200];
    char OutputDir[200];
    char SourceFile[200];
    double HydrogenFraction;
    
    int n_iters;
    long n_photons_star;
    long n_photons_uvb;
    
    double UnitMass_in_g;
    double UnitLength_in_cm;
    double UnitVelocity_in_cm_per_s; // units
    
    double J_uvb; // UVB intensity
    double dust_kappa; // dust opacity
    
    int n_mu;
    int n_phi; // number of orientations to track
} All;

// octree
long TOTAL_NUMBER_OF_CELLS;
long TOTAL_NUMBER_OF_PARENTS;
long TOTAL_NUMBER_OF_LEAVES;
double MAX_DISTANCE_FROM0;

long *LEAF_IDS;
long *PARENT_IDS;

#define N_SUBCELL   2
#define N_SUBCELL_2 4
#define N_SUBCELL_3 8

typedef struct LOCALVALS_s
{
    double rho;             /* density */
    double nh;              /* HI fraction */
    double z;               /* metallicity */
    double T;               /* temperature */
    double J_UV;              /* HI ionization rate, in # cm^3 s^-1 */
} LOCALVAL;

struct CELL_STRUCT
{
    double width;           /* width from one edge to another of the cell */
    double min_x[3];        /* x,y,z minima of the cell */
    
    long parent_ID;         /* ID number of parent cell for the given cell */
    long sub_cell_check;    /* = 1 if there are sub-cells, 0 if not */
    long *sub_cell_IDs;     /* ID numbers of the sub-cells contained (if present) */
    
    LOCALVAL *U;            /* structure to hold local variables of the cell */
} *CELL;

// sources and CDF
struct SOURCE_s
{
    double pos[3];
    double Q;       /* number of ionizing photons */
    long cell_id;   /* ID of the cell it lives in */
    
    long Nemit;     /* number of photon packages emitted */
    long Nescape;   /* number of photon packages escaped */
} *SOURCE;

typedef struct CDF_s
{
    long n;
    double Q;
    double w;   // re-weight photon sampling
    double *pdf;
    double *cdf;
} CDF;

CDF CDF_source;

double Q_uvb; // total number of photons from the UVB

// ray structure
typedef struct Ray_s
{
    double pos[3];				/* current position */
    long cell_id;               /* current cell */
    long source_id;             /* id of the source emitted*/
    
    double mu;                  /* polar angle theta of the ray */
    double phi;                 /* azimuthal angle phi of the ray */
    double n_hat[3];			/* unit vector direction of the ray */
    
    double Q;					/* number of ionizing photons it represents */
    double E;                   /* photon energy */
    
    int escape;                 /* if escaped */
    int killed;                 /* if killed */
    int source;                 /* if from source (not UVB) */
} Ray_struct;

// output spectrum
double* spectrum;
