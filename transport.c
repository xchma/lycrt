#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gsl/gsl_rng.h>

#include "allvar.h"
#include "proto.h"


void carry_ray_for_one_step(Ray_struct *ray)
{
    long j, cell_id = ray->cell_id;
    double stepsize = maxstep_escape_cell(ray->pos, ray->n_hat, ray->cell_id);
    double this_step = stepsize;
    int scatter = -1;
    
    // photo-ionization opacity
    double alpha_pi = 6.304e-18*pow(pc.chi/ray->E,3);
    double gas_opac = alpha_pi*(All.HydrogenFraction*CELL[cell_id].U->rho/pc.mp)*CELL[cell_id].U->nh;
    
    // dust opacity
    double dust_opac = 0.4*All.dust_kappa*CELL[cell_id].U->rho*CELL[cell_id].U->z;
    
    // random optical depth and step size
    double tot_opac = dust_opac + gas_opac;
    double tau_r = -log(1-gsl_rng_uniform(random_generator));
    double l_step = tau_r/tot_opac;
    
    // find the next event
    if (l_step<stepsize) {this_step=l_step; scatter=1;}
    
    // take the step
    for (j=0;j<3;j++) ray->pos[j] += this_step*ray->n_hat[j];
    CELL[cell_id].U->J_UV += ray->Q*this_step*alpha_pi;
    
    if (scatter==1)
    {
        if (gsl_rng_uniform(random_generator)<dust_opac/tot_opac) // interact with dust
        {
            if (gsl_rng_uniform(random_generator)<0.277) // dust scatter
            {
                ray->mu = -1+2.0*gsl_rng_uniform(random_generator);
                ray->phi = 2.0*pc.pi*gsl_rng_uniform(random_generator);
                double sin_theta = sqrt(1-ray->mu*ray->mu);
                ray->n_hat[0] = sin_theta*cos(ray->phi);
                ray->n_hat[1] = sin_theta*sin(ray->phi);
                ray->n_hat[2] = ray->mu;
            }
            else // dust absorption
            {
                ray->killed = 1;
                return;
            }
        }
        else // interact as recombination to ground state
        {
            //if (gsl_rng_uniform(random_generator)<0.38) // case A
            if (gsl_rng_uniform(random_generator)<0) // case B
            {
                ray->mu = -1+2.0*gsl_rng_uniform(random_generator);
                ray->phi = 2.0*pc.pi*gsl_rng_uniform(random_generator);
                double sin_theta = sqrt(1-ray->mu*ray->mu);
                ray->n_hat[0] = sin_theta*cos(ray->phi);
                ray->n_hat[1] = sin_theta*sin(ray->phi);
                ray->n_hat[2] = ray->mu;
                ray->E = 1.1*pc.chi; // re-emit just above threshold (see W&L 2000)
            }
            else
            {
                ray->killed = 1;
                return;
            }
        }
    }
    
    // check if escaped
    ray->cell_id = find_cell_from_cell(ray->pos, cell_id);
    if (ray->cell_id==-1) {ray->killed=1; ray->escape=1;}
}


void integrate_ray_to_escape(Ray_struct *ray)
{
    while (ray->killed==0){
        carry_ray_for_one_step(ray);
    }
    
    if ((ray->escape==1) && (ray->source==1))
    {
        long source_id = ray->source_id;
        SOURCE[source_id].Nescape++;
        
        // record to spectrum
        int i_mu = (int)((double)All.n_mu*(1-ray->mu)/2.0);
        int i_phi = (int)((double)All.n_phi*(ray->phi/(2*pc.pi)));
        spectrum[i_mu*All.n_phi+i_phi] += ray->Q;
    }
}
